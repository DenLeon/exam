'use strict';



const gulp = require('gulp'),
    rimraf = require('rimraf'),
    scss = require('gulp-sass'),
    uglify = require('gulp-uglify'),
    prefixer = require('gulp-autoprefixer'),
    htmlmin = require('gulp-htmlmin'),
    browserSync = require('browser-sync'),
    plumber = require('gulp-plumber'),
    reload = browserSync.reload;

// Пути к папке dist

var path = {
        dist:{
            all:'dist/',
            html:'dist/',
            scss:'dist/css/',
            js:'dist/js/',
            img:'dist/img/'
        },
        src:{
            html:'src/*.{html,htm}',
            scss:'src/scss/style.scss',
            js:'src/js/main.js',
            jsplugins:'src/js/plugins/*.js',
            img:'src/img/**/*.{jpg,gif,png,svg,jpeg}'
        },
        watch:{
            scss:'src/scss/.scss',
            js:'src/js/**/*.js',
            html:'src/*.{html,htm}',
            img:'src/img/**/*.{jpg,gif,png,svg,jpeg}'
        },
        clean:'dist/'
    },
    config={
        server:{
            baseDir:"./dist",
            index:"index.html"
        },
        host:"localhost",
        port:7787,
        tunnel:true,
        logPrefix:"Automatization_Exam"
    };

// Чистка папки

gulp.task('clean',function (done) {
    rimraf(path.clean,done);
});

// запуск webserver-браузер

gulp.task('webserver',function (done) {
    browserSync(config);
    done();
});

/* Задания */

// html

gulp.task('dev:html',function (done) {
    gulp.src(path.src.html)
        .pipe(gulp.dest(path.dist.html))
        .pipe(reload({stream:true}));
    done();
});

// scss

gulp.task('dev:scss', function (done) {
    gulp.src(path.src.scss,{sourcemaps: true})
        .pipe(plumber())
        .pipe(scss({
            // путь к файлам bourbon
            includePaths:'src/scss/bourbon/bourbon/',
            outputStyle:"compressed",
            sourcemaps:false
        }))
        .pipe(prefixer({
            cascade:false,
            remove:true
        }))
        .pipe(gulp.dest(path.dist.scss,{sourcemaps:'.'}))
        .pipe(reload({stream:true}));
    done();
});

//  js


gulp.task('dev:js', function (done) {
    gulp.src(path.src.js,{sourcemaps: true})
        .pipe(gulp.dest(path.dist.js,{sourcemaps:'.'}))
        .pipe(reload({stream:true}));
    done();
});


/* все задачи по окультуриванию кода */

// сжатие html

gulp.task('prod:html',function (done) {
    gulp.src(path.src.html)
        .pipe(htmlmin({
            collapseWhitespace:true
        }))
        .pipe(gulp.dest(path.dist.html));
    done();
});

// сжатие css и добавление префиксов

gulp.task('prod:scss', function (done) {
    gulp.src(path.src.scss)
        .pipe(scss({
            includePaths:'src/scss/bourbon/bourbon/',     // путь к файлам bourbon
            outputStyle:"compressed",
            sourcemaps:false
        }))
        .pipe(prefixer({
            cascade:false,
            remove:true
        }))
        .pipe(gulp.dest(path.dist.scss));
    done();
});

// сжимаем js

gulp.task('prod:js', function (done) {
    gulp.src(path.src.js)
        .pipe(uglify())
        .pipe(gulp.dest(path.dist.js));
    done();
});

// перенос картинок в dist

gulp.task('mv:img', function (done) {
    gulp.src(path.src.img)
        .pipe(gulp.dest(path.dist.img))
    done();
});

// копирование js плагинов в dist и их сжатие

gulp.task('js:addons', function (done){
    gulp.src(path.src.jsplugins)
        .pipe(uglify())
        .pipe(gulp.dest('dist/js/'));
    done();
});

// копирование css плагинов в dist и их сжатие

gulp.task('css:addons', function (done){
    gulp.src('src/scss/*.scss')
        .pipe(scss({
            outputStyle:"compressed",
            sourcemaps:false
        }))
        .pipe(gulp.dest('dist/css/'));
    done();
});

// отслеживание изменений

gulp.task('watch', function (done) {
    gulp.watch(path.watch.html,gulp.series('dev:html'),reload({stream:true}));
    gulp.watch(path.watch.js,gulp.series('dev:js'),reload({stream:true}));
    gulp.watch(path.watch.scss,gulp.series('dev:scss'),reload({stream:true}));
    done();
});

gulp.task('prod',gulp.series('clean',gulp.parallel('css:addons','mv:img','js:addons','prod:html','prod:scss','prod:js'),'webserver'));
gulp.task('dev',gulp.series('clean',gulp.parallel('css:addons','mv:img','js:addons','dev:html','dev:scss','dev:js'),'webserver','watch'));

gulp.task('default',gulp.series('dev'));